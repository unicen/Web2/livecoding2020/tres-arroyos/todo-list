<!DOCTYPE html>
<html lang="en">
<head>
    <base href="{BASE_URL}">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TODOList</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <!-- development version, includes helpful console warnings -->
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

</head>
<body>

    {include 'header.tpl'}

    <main class="container"> <!-- inicio del contenido pricipal -->

        <div class="row">
            <div class="col-md-4">
                {include 'form_alta.tpl' upload=false}
            </div>

            <div class="col-md-8">
                {include file="vue/taskList.vue"}
            </div>
        </div>


    </main>

    <!-- incluyo JS para CSR -->
    <script src="js/tasks.js"></script>

    {include file="footer.tpl"}
</body>
</html>    