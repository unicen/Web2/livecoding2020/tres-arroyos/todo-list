<!-- formulario de alta de tarea -->
<form id="task-form" action="insertar" method="POST" class="my-4" enctype="multipart/form-data">
    <div class="row">
        <div class="col-9">
            <div class="form-group">
                <label>Título</label>
                <input name="titulo" type="text" class="form-control">
            </div>
        </div>

        <div class="col-3">
            <div class="form-group">
                <label>Prioridad</label>
                <select name="prioridad" class="form-control">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label>Descripcion</label>
        <textarea name="descripcion" class="form-control" rows="3"></textarea>
    </div>


    {if isset($upload) && $upload}
    <div class="form-group">    
        <label>Imagen</label>
        <input type="file" name="input_name" id="imageToUpload">
    </div>
    {/if}

    <button type="submit" class="btn btn-primary">Guardar</button>
</form>
