<?php
require_once 'app/models/task.model.php';
require_once 'app/api/api.view.php';

class ApiTaskController {

    private $model;
    private $view;

    function __construct() {
        $this->model = new TaskModel();
        $this->view = new APIView();
        $this->data = file_get_contents("php://input");
    }

    // Lee la variable asociada a la entrada estandar y la convierte en JSON
    function getData(){ 
        return json_decode($this->data); 
    } 

    public function getAll($params = null) {
        $parametros = [];

        if (isset($_GET['sort'])) {
            $parametros['sort'] = $_GET['sort'];
        }

        if (isset($_GET['order'])) {
            $parametros['order'] = $_GET['order'];
        }

        $tasks = $this->model->getAll($parametros);
        $this->view->response($tasks, 200);
    }

    public function get($params = null) {
        // $params es un array asociativo con los parámetros de la ruta
        $idTask = $params[':ID'];
        $task = $this->model->get($idTask);
        if ($task)
            $this->view->response($task, 200);
        else
            $this->view->response("La tarea con el id=$idTask no existe", 404);
    }

    public function delete($params = null) {
        $idTask = $params[':ID'];
        $success = $this->model->remove($idTask);
        if ($success) {
            $this->view->response("La tarea con el id=$idTask se borró exitosamente", 200);
        }
        else { 
            $this->view->response("La tarea con el id=$idTask no existe", 404);
        }
    }

    public function add($params = null) {

        $body = $this->getData();

        $titulo       = $body->titulo;
        $descripcion  = $body->descripcion;
        $prioridad    = $body->prioridad;

        $id = $this->model->insert($titulo, $descripcion, $prioridad);

        if ($id > 0) {
            $task = $this->model->get($id);
            $this->view->response($task, 200);
        }
        else { 
            $this->view->response("No se pudo insertar", 500);
        }
    }

    public function update($params = null) {
        $idTask = $params[':ID'];
        $body = $this->getData();

        $titulo       = $body->titulo;
        $descripcion  = $body->descripcion;
        $prioridad    = $body->prioridad;

        $success = $this->model->update($titulo, $descripcion, $prioridad, $idTask);

        if ($success) {
            $this->view->response("Se actualizó la tarea $idTask exitosamente", 200);
        }
        else { 
            $this->view->response("No se pudo actualizar", 500);
        }
    }

    public function show404($params = null) {
        $this->view->response("El recurso solicitado no existe", 404);
    }

}