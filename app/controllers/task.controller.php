<?php
include_once 'app/models/task.model.php';
include_once 'app/views/task.view.php';
include_once 'app/helpers/auth.helper.php';

class TaskController {

    private $model;
    private $view;
    private $authHelper;

    function __construct() {
        $this->model = new TaskModel();
        $this->view = new TaskView();
        $this->authHelper = new AuthHelper();

        // verifico que el usuario esté logueado siempre
        $this->authHelper->checkLogged();
    }

    /**
     * Imprime la lista de tareas
     */
    function showTasks() {
        // obtiene las tareas del modelo
        $tasks = $this->model->getAll();

       // actualizo la vista
       $this->view->showTasks($tasks);
    }

    /**
     * Imprime la pantalla principal de tareas para CSR
     */
    function showTasksCSR() {
       $this->view->showTasksCSR();
    }

    /**
     * Muestra el detalle del item
     * @param $id
     */
    function showDetail($id) {
        $task = $this->model->get($id);
        if($task) {
            $this->view->showTask($task);
        }
        else {
            $this->view->showError('Tarea no encontrada');
        }
    }

    /**
     * Construye un nombre unico de archivo y ademas lo mueve a 
     * mi carpeta de imagenes
     */
    function uniqueSaveName($realName, $tempName) {
        
        $filePath = "images/" . uniqid("", true) . "." 
            . strtolower(pathinfo($realName, PATHINFO_EXTENSION));

        move_uploaded_file($tempName, $filePath);

        return $filePath;
    }

    /**
     * Inserta una tarea en el sistema
     */
    function addTask() {
        $titulo = $_POST['titulo'];
        $descripcion = $_POST['descripcion'];
        $prioridad = $_POST['prioridad'];

        // verifico campos obligatorios
        if (empty($titulo) || empty($prioridad)) {
            $this->view->showError('Faltan datos obligatorios');
            die();
        }

        // inserto la tarea en la DB
        if($_FILES['input_name']['type'] == "image/jpg" || 
            $_FILES['input_name']['type'] == "image/jpeg" || 
            $_FILES['input_name']['type'] == "image/png" ) 
        {
            $realName = $this->uniqueSaveName($_FILES['input_name']['name'], 
                                                $_FILES['input_name']['tmp_name']);

            $id = $this->model->insert($titulo, $descripcion,  $prioridad, $realName);
        }
        else {
            $id = $this->model->insert($titulo, $descripcion,  $prioridad);
        }


        // redirigimos al listado
        header("Location: " . BASE_URL); 
    }

    /**
     * Elimina la tarea del sistema
     */
    function deleteTask($id) {
        $this->model->remove($id);
        header("Location: " . BASE_URL); 
    }

    function finalizeTask($id) {
        $this->model->finalize($id);
        header("Location: " . BASE_URL);
    }

}