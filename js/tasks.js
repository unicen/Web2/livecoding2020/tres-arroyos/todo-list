"use strict"

const app = new Vue({
    el: "#app",
    data: {
        tareas: [], // esto es como un assign de smarty
    }, 
});

document.addEventListener('DOMContentLoaded', e => {
    getTasks();

    document.querySelector('#task-form').addEventListener('submit', e => {
        e.preventDefault();
        addTask();
    });

});

async function getTasks() {
    try {
        const response = await fetch('api/tareas');
        const tasks = await response.json();
        
        // imprimo las tareas
        app.tareas = tasks;

    } catch(e) {
        console.log(e);
    }
}


async function addTask() {

    // armo la tarea
    const task = {
        titulo: document.querySelector('input[name=titulo]').value,
        descripcion: document.querySelector('textarea[name=descripcion]').value,
        prioridad: document.querySelector('select[name=prioridad]').value,
        finalizada: false
    }

    try {
        const response = await fetch('api/tareas' , {
            method: 'POST',
            headers: {'Content-Type': 'application/json'}, 
            body: JSON.stringify(task)
        });

        const t = await response.json();
        app.tareas.push(t);

    } catch(e) {
        console.log(e);
    }


}


